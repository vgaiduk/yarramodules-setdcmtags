#!/usr/bin/env bash

rm -r build
mkdir build
cd build
CXX="g++" CC="gcc" cmake ..
make all
