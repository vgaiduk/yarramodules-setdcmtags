SetDCMTag is Yarra postprocessing module that reads all relevant patient / scan information from the .dat measurement 
file and inserts DICOM tags into all DICOM files created by the reconstruction module.

# Contributors

Martijn Cloos, Steven Baete, Florian Knoll: [raw2dicom Matlab class](https://bitbucket.org/sbaete/raw2dicom)

Kaveh Vahedipour: [protpp C++ library](https://bitbucket.org/kvahed/protpp)

Vadim Gaiduk, Tobias Block

The software makes use of and is based on [DCMTK - DICOM Toolkit](http://dcmtk.org/) developped by

    OFFIS e.V.
    R&D Division Health
    Escherweg 2
    26121 Oldenburg, Germanyу
 

# License Information #
The Yarra framework is provided free of charge for use in research applications. It must not be used 
for diagnostic applications. The author takes no responsibility of any kind for the accuracy or integrity 
of the created data sets. No data created using the framework should be used to make diagnostic decisions. The Yarra framework, including all of its software components, comes without any warranties. 
Operation of the software is solely on the user’s own risk. The author takes no responsibility for damage 
or misfunction of any kind caused by the software, including permanent damages to MR systems, temporary 
unavailability of MR systems, and loss or corruption of research or patient data. 
This applies to all software components included in the Yarra software package.

The source is released under the GNU General Public License, GPL (http://www.gnu.org/copyleft/gpl.html). 
The source code is provided without warranties of any kind.

Usage
=====

SetDCMTag [options] [in-dcmfile] [out-dcmfile]

Description
-----------
`SetDCMTag` reads raw measurement file created by Siemens MRI scanners and writes discovered tags to the existing DICOM files.

The main use case for `SetDCMTag` is to run it as a post-processing tool after reconstructing the image 
with a custom algorithm written e.g. in Matlab, so that the algorithm can be unaware of Siemens-specific tags that shall be
copied or transformed from raw files to the reconstructed DICOMs.   
The algorithm can just save the images with `dicomwrite()` functions without providing additional options.

If both `in-dcmfile` and `out-dcmfile` are provided, then `SetDCMTag` copies DICOM file(s) from `in-dcmfile` to `out-dcmfile`.
If only `in-dcmfile` is provided then tags are written to the existing DICOM files.    
Both `in-dcmfile` and `out-dcmfile` can be directory or file paths. If `in-dcmfile` is a file path and `out-dcmfile` is 
a directory path then `in-dcmfile` file is copied with tags added, to `out-dcmfile` directory.

The raw measurement file is provided with `--raw-file` mandatory option.    
Dumping options can be provided (see *raw file tags dump options*) to write all available raw file tags 
in a human-readable format. Such an option can be provided with or without file name. 
In a latter case, a format-specific extension will be added to a raw file name to form a dump file name.

If `in-dcmfile` and `out-dcmfile` are omitted then `SetDCMTag` reads the raw measurement file and, if any of the dumping
options are provided (see *raw file tags dump options*), writes the corresponding dump.

To customize which tags shall be read from the raw file and written to DICOMs, up to two ini files can be provided with 
`--use-ini` and `--additional-ini` options. See *INI-file structure* section below for the details.

An additional DICOM tag dictionary can be provided with `--dict-file` option; it shall be a text file in [DICOM toolkit 
dictionary format](http://support.dcmtk.org/docs/file_datadict.html)

**Important Note:** series and slice numbers are critical for PACS and DICOM viewer applications. 
They are discovered from original DICOM file names using a default 
[regex](http://www.boost.org/doc/libs/1_59_0/libs/regex/doc/html/boost_regex/syntax/perl_syntax.html) 
that can be changed with `--dcmfile_regex` options.

parameters:
-----------

`in-dcmfile`
DICOM input file or directory name

`out-dcmfile`
DICOM output file or directory name to write to; if empty, the tags are written to `in-dcmfile`


general options:
----------------

`-h`     
`--help`                 
print this help text and exit
     
`--version`              
print version information and exit
     
`--arguments`            
print expanded command line arguments

`-q`     
`--quiet`                
quiet mode, print no warnings and errors

`-v`     
`--verbose`              
verbose mode, print processing details

`-d`     
`--debug`                
debug mode, print debug information

`-ll` *level*   
`--log-level` *level*   
use level *level* (fatal, error, warn, info, debug, trace) for the logger
                             
`-lc` *filename*   
`--log-config` *filename*   
use config file *filename* for the logger
    
processing options:
-------------------

`-r` *filename*   
`--raw-file` *filename*   
raw .dat file name to read tag values from

`-n` *number*   
`--acc-number` *number*   
ACC number

`-e`   
`--stop-on-error`        
Stop processing on error; default behaviour is to report an error and to continue

`-x` *regex*   
`--dcmfile_regex` *regex*   
*regex* used for parsing dcm file name to define series (capture name: 'series') and slice (capture name: 'slice');   
default is  `(?'name'\\w*)\\.(?'series'\\d*)\\.(?'slice'\\d*).dcm`

`-m` *filename*   
`--dict-file` *filename*
additional DICOM dictionary file

`-i`  *filename*  
`--use-ini` *filename*      
use ini file to load tag lookup table

`-a`  *filename*  
`--additional-ini` *filename*
additional ini file (e.g. located in the data directory) to load tag lookup table; 
can overwrite the values loaded by `-i` option
    
`-l`   
`--log-tag-values`   
log tag and varialbe values to the file *out_file_name*.dcm.log


raw file tags dump options:
---------------------------

`-wx` [*filename*]  
`--write-xml` [*filename*]   
write tag dump in XML format; default: *datfile*.xml

`-wi` [*filename*]   
`--write-info` [*filename*]   
write tag dump in INFO format; default: *datfile*.info

`-wn` [*filename*]   
`--write-ini` [*filename*]   
write tag dump in INI format; default: *datfile*.ini

`-wt` [*filename*]  
`--write-txt` [*filename*]  
write tag dump in TXT format; default: *datfile*.txt


INI-file structure
==================

To customize which tags shall be read from the raw file and written to DICOMs, up to two ini files can be provided with 
`--use-ini` and `--additional-ini` options. See *INI-file structure* section below for the details.

Section name(s)
---------------

The sections recognized by `SetDCMTag` shall be named as:

    [SetDCMTags.series.slice]
    
All other sections of the INI file are ignored. 

`series`
The series identifier to which the tags shall be only applyed, if not omitted   
Shall have exactly the same format as in the file name, i.e. with the same number of leading zeroes

`slice`
The slice identifier to which the tags shall be only applyed, if not omitted
Shall have exactly the same format as in th file name, i.e. with the same number of leading zeroes

If `.slice` or `.series.slice` part of the section name is omitted, then the section is applied to all slices or series.

    
Values:
-------

The lines in the `[SetDCMTag]` section shall have the following structure

    dcmtag = dattag, value, variable

Any or all of dattag, value, variable parts can be empty. All lines starting with semicolon `;` are ignored.   

`dcmtag`   
The DICOM tag name to be stored in the DCM file; the entry is ignored if it is empty

`dattag`   
Raw data file tag path; can contain wildcards (.*. for any leaf name in the path or .**. for few leafs); can be empty

`value`
Constant value to be written to DICOM tag. When read from file contains the default value that can be overwritten by other sources

`variable`
Name of pre-calculated variable; if set, its value will be used to write to DICOM tag

Example:
--------
    [SetDCMTags.02]
    ColorType = , truecolor
    PhotometricInterpretation = , RGB