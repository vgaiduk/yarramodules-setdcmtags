#!/usr/bin/env bash

rm -r build
mkdir build
cd build
CXX="g++-5" BOOST_ROOT="~/Projects-Cai2R/boost_1_59_0" cmake ..
make all
