//
// Created by Vadim on 10/26/15.
//

#ifndef SETDCMTAGS_TAGSLOOKUPTABLE_H
#define SETDCMTAGS_TAGSLOOKUPTABLE_H

#include <string>
#include <deque>
#include <map>
#include <vector>

/** This class holds a lookup table of DICOM tag names, their (default) values, raw data tags and calculated variables
 * that can be a source of the tag value.
 * The class holds and provides a method to calculate variables used in the lookup table
 */
class TagsLookupTable {

public:
    /// Predefined (not-calculated) variable name. This variable instructs to keep the DICOM tag value
    /// in the source file instead of overwriting it by any value
    static constexpr const char * variableKeep = "keep";

    /// Predefined (not-calculated) variable name. This variable instructs to generate a new UID to store as a DICOM tag value
    static constexpr const char * variableUID = "uid";

    /// Predefined (not-calculated) variable name. This variable instructs to generate a new series UID, if necessary,
    // or use the existing one, to store it as a DICOM tag value
    static constexpr const char * variableSeriesUID = "series-uid";

    /// Predefined (not-calculated) variable name. This variable instructs to generate a unque-per-directory study UID,
    // if necessary, to store it as a DICOM tag value
    static constexpr const char * variableStudyUID = "study-uid";

    /// Predefined variable name. The value is set when the file name is parsed by parseFilename
    static constexpr const char *variableSeries = "series";

    /// The default value for the series variable used when the series is missing from parsing the file name
    static constexpr const char *defaultSeriesValue = "2000";

    /// Predefined variable name. The value is set when the file name is parsed by parseFilename
    static constexpr const char * variableSlice = "slice";

    /// The DICOM tag lookup table entry
    struct dcmtag_entry_t {
        /// The DICOM tag name to be stored in the DCM file; the entry is ignored if it is empty
        std::string dcmtag;

        /// Raw data file tag path; can contain wildcards
        /// (.*. for any leaf name in the path or .**. for few leafs); can be empty
        std::vector<std::string> dattag;

        /// Constant value to be written to DICOM tag
        /// When read from file contains the default value that can be overwritten by other sources
        std::string value;

        /// Name of pre-calculated variable; if set, its value will be used to write to DICOM tag
        std::string variable;

        /// The sequence identifier to which this tag shall be only applyed, if not empty
        /// Shall have exactly the same format as in th file name, i.e. with the same number of leading zeroes
        std::string sequence;

        /// The slice identifier to which this tag shall be only applyed, if not empty
        /// Shall have exactly the same format as in th file name, i.e. with the same number of leading zeroes
        std::string slice;

        /** Constructor, initializes member-variables
        */
        dcmtag_entry_t(const std::string & _dcmtag,
                       const std::string & _dattag = "",
                       const std::string & _value = "",
                       const std::string & _variable = "",
                       const std::string & _sequence = "",
                       const std::string & _slice = "")
                : dcmtag(_dcmtag), dattag{_dattag}, value(_value), variable(_variable), sequence(_sequence), slice(_slice)
        {}

        /** Constructor, initializes member-variables
        */
        dcmtag_entry_t(const std::string & _dcmtag,
                       const std::vector<std::string> & _dattag,
                       const std::string & _value = "",
                       const std::string & _variable = "",
                       const std::string & _sequence = "",
                       const std::string & _slice = "")
                : dcmtag(_dcmtag), dattag(_dattag), value(_value), variable(_variable), sequence(_sequence), slice(_slice)
        {}
    };

    /** Lookup table holding DICOM tag names, their (default) values, raw data tags and calculated variables
     */
    typedef std::deque<dcmtag_entry_t> tag_entries_t;
    tag_entries_t tag_entries;

    /** Map holding variables and their (calculated) values that can be used in the lookup
     *  table to define the way how the DICOM tag value is found
     */
    typedef std::map<std::string, std::string> tag_variables_t;
    tag_variables_t tag_variables;

    /** Map holding mrprot tags and their values
     */
    typedef std::map<std::string, std::string> mrprot_tags_t;
    mrprot_tags_t mrprot_tags;

    /** Map holding the uids generated per series; it allows to use the same series uid for all DICOMs
     *  belonging to the same series
     */
    std::map<std::string, std::string> series_uids;

    /** Study uid to be set the same for the whole import
     */
    std::string study_uid;

    /** Constructor, initializes `tag_entries` with `dcmtags[]` array (see `dcmtags.hpp` file)
     *  @param accessionNumber Accession Number provided in the command line
     *  to be stored as `accessionNumber` variable in `tag_variables`
     */
    TagsLookupTable(const char * accessionNumber);

    /** Loads `tag_entries` entries from specified INI file.
     * The INI file shall contain [SetDCMTags] section (all other sections are ignored).
     * Each value in the section has format:
     * `<dcmtag> = <dattag>, <value>, <variable>`
     * where `<dcmtag>`, `<dattag>`, `<value>` and `<variable>` are loaded to `dcmtag_entry_t` fields.
     * The values are separated by ','; the spaces and quotation marks surrounding values are ignored
     * Example:
     *     [SetDCMTags]
     *     PatientID = XProtocol.Config.ParamMap.PARC.RECOMPOSE.PatientID
     *     ContentDate = , , dateofseccapture
     *     CSAImageHeaderType = , "IMAGE NUM 4"
     *
     * @param fname INI file name to read
     * @param overwrite if true, overwrite the lookup table by the  entries from the INI file
     * instead of appending/updating values in the table
     * @param tagEntriesSectionName the name of the INI file section to be read; all other sections are ignored
     */
    void loadFromINI(const char * fname, bool overwrite = false, const char * tagEntriesSectionName = "SetDCMTags");

    /** Prints DICOM tags with corresponding value in human-readable format
     *  @param stream to write to
     */
    void printDcmTags(std::ostream &stream) const;

    /** Prints DICOM tags with corresponding value in human-readable format
     *  @param fname file name to write to
     */
    void printDcmTags(const std::string &fname) const;

    /** Prints avalable (calculated) variable names and values in human-readable format
     *  @param stream to write to
     */
    void printTagVariables(std::ostream &stream) const;

    /** Iterates through the lookup table and assigns tag values based on variable name, if set
     *  @param log_stream stream to log the errors in readable format
     *  @return returns false if some of the variables was missing, true otherwise
     */
    bool applyTagVariables(std::ostream &log_stream);

    /// Default regex to be used for file name parsing
    constexpr static const char * default_dcmfile_regex = "(?'name'\\w*)\\.(?'seq'\\d*)\\.(?'slice'\\d*).dcm";

    /** Parses the provided file name to find and store the values in tag_variables for the following variables:
     * - `seriesName` from the capture `name`
     * - `sequence` from the capture `seq`
     * - `slice` from the capture `slice`
     *  @param log_stream stream to log the errors in readable format
     *  @param fname file name to be parsed
     *  @param regex regular expression containing `name`, `seq` and `slice` captures used to process the file name
     *  @return returns false if the parse failed, true otherwise
     */
    bool parseFilename(std::ostream &log_stream, const std::string & fname, const char * regex = default_dcmfile_regex);

    /** Calculates variables related to the image orientation
     *  @param log_stream stream to log the errors in readable format
     *  @return returns true if all necessary dattad were found and variable calculation was successful, false otherwise
     */
    bool calcImageOrientation( std::ostream & log_stream);

    /** Prints avalable mrprot tag names and values in human-readable format
     *  @param stream to write to
     */
    void printMRprotTags(std::ostream &stream) const;

    /** Prints avalable mrprot tag names and values in human-readable format
     *  @param fname file name to write to
     */
    void printMRprotTags(const std::string &fname) const;

    /** Reads an mrprot tag value
     *  @param dattag Raw data file tag name to be read
     *  @param[out] Read value
     *  @param log_stream stream to log the errors in readable format
     *  @param default value set if the dattag was not found
     *  @return returns true if the dattad was found and read successfully, false otherwise
     */
    bool getMRprotValue(const char *dattag, double & value, std::ostream & log_stream, double default_value = 0);

    /** Reads an mrprot tag value
     *  @param dattag Raw data file tag name to be read
     *  @param[out] Read value
     *  @param log_stream stream to log the errors in readable format
     *  @return returns true if the dattad was found and read successfully, false otherwise
     */
    bool getMRprotValue(const char * dattag, std::string &value, std::ostream &log_stream);

    /** Reads an mrprot tag value
     *  @param dattags Raw data file tag names to be read; first tag name found in the raw file sets the value
     *  @param[out] Read value
     *  @param log_stream stream to log the errors in readable format
     *  @return returns true if the dattad was found and read successfully, false otherwise
     */
    bool getMRprotValue(const std::vector<std::string>& dattags, std::string &value, std::ostream & log_stream);

    /** Reads an mrprot tag value
     *  @param dattags Raw data file tag names to be read; first tag name found in the raw file sets the value
     *  @param[out] Read value
     *  @param log_stream stream to log the errors in readable format
     *  @param default value set if the dattag was not found
     *  @return returns true if the dattad was found and read successfully, false otherwise
     */
    bool getMRprotValue(const std::vector<std::string>& dattags, double &value, std::ostream &log_stream, double default_value = 0);


};

/** Cleans the value by removing leading and trailing spaces as well as quote marks surrounding the value
 *  @param value string to cleared
 *  @return returns the cleared string with removed leading and trailing spaces
 *  as well as quotation marks surrounding the value
 */
std::string cleanStringValue (const std::string & value);

#endif //SETDCMTAGS_TAGSLOOKUPTABLE_H
