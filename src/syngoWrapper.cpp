//
// Created by Vadim on 10/23/15.
//


#include "syngoWrapper.h"

#include "splitString.hpp"

#include <iomanip>

#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/regex.hpp>

#include <boost/format.hpp>


SyngoWrapper::SyngoWrapper(const char * fname)
        :  smprot (false, fname)
{
};

void SyngoWrapper::printSyngoTags(std::ostream &stream) {

    printTree(smprot.Properties(), stream);
}

void SyngoWrapper::printSyngoTags(const std::string &fname) {

    std::basic_ofstream<boost::property_tree::ptree::key_type::value_type> stream(fname);
    printSyngoTags(stream);
}


template <typename Tree, typename T = std::string>
bool enumerate_path(Tree const& pt, typename Tree::path_type path, T & out) {
    if (path.empty())
        return false;

    if (path.single()) {
        out = pt.template get<T>(path);
        return true;
    } else {
        auto orig_path = path;
        auto head = path.reduce();
        for (auto& child : pt) {
            if (head == "*" || head == "**" || child.first == head) {
                if(enumerate_path(child.second, path, out))
                    return true;
            }
            if (head == "**") {
                if(enumerate_path(child.second, orig_path, out))
                    return true;
            }
        }
    }
    return false;
}


bool SyngoWrapper::getTagValue(const char *dattag, std::string &value, std::ostream & log_stream) {
    bool success = true;
    if (dattag && strlen(dattag)) {
        try {
            value = cleanStringValue(smprot.GetStr(dattag));
        } catch (const boost::property_tree::ptree_bad_path & e) {
            //try to read with wildcards
            std::string outs;
            try {
                if (enumerate_path(smprot.Properties(), dattag, outs)) {
                    value = cleanStringValue(outs);
                } else {
                    success = false;
                    log_stream << "Error reading raw data tag [" << dattag << "]: " << e.what() << std::endl;
                }
            } catch (const boost::property_tree::ptree_bad_path & e) {
                success = false;
                log_stream << "Error reading raw data tag [" << dattag << "]: " << e.what() << std::endl;
            }
        }
    }
    return success;
}


bool SyngoWrapper::getTagValue(const std::vector<std::string>& dattags, std::string &value, std::ostream & log_stream) {

    std::ostringstream log;
    for (const auto & s : dattags) {
        if (getTagValue(s.c_str(), value, log)) {
            return true;
        }
    }
    log_stream << log.str();
    return false;
}

bool SyngoWrapper::getTagValue(const char *dattag, double &value, std::ostream &log_stream, double default_value) {

    value = default_value;
    std::string tmp_str;
    if (getTagValue(dattag, tmp_str, log_stream)) {
        try {
            value = stod(tmp_str);
            return true;
        } catch (...) {
            log_stream << "Cannot read tag [" << dattag << "] as double: " << tmp_str << std::endl;
        }
    }
    return false;
}

bool SyngoWrapper::processDcmTags(TagsLookupTable::tag_entries_t &tag_entries, std::ostream &log_stream) {

    bool success = true;
    for (auto & entry : tag_entries) {

        success &= getTagValue(entry.dattag, entry.value, log_stream);
    }
    return success;
}

void SyngoWrapper::printSyngoTagsToINI(const std::string &fname) {
    write_ini(fname, smprot.Properties());
}

void SyngoWrapper::printSyngoTagsToXML(const std::string &fname) {
    smprot.ToXML(fname);
}

void SyngoWrapper::printSyngoTagsToINFO(const std::string &fname) {
    write_info(fname, smprot.Properties());
}

bool SyngoWrapper::calcDcmtagVariables(TagsLookupTable::tag_variables_t & tag_variables, std::ostream & log_stream) {


    bool success = true;

    // Lines 41:70 of makeHeaders.m

//    % prep
//    FrameOfReference = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.FrameOfReference;
//    ind = strfind(FrameOfReference,'.');

    //"1.3.12.2.1107.5.2.38.51022.1.20150205123347593.0.0.4980"
    std::string FrameOfReference;
    success &= getTagValue("XProtocol.**.acquisition.FeedbackRoot.FrameOfReference", FrameOfReference, log_stream);

//    DICOM_UID_ROOT = FrameOfReference(1:ind(9));
//    daystr = FrameOfReference((ind(10)+(1:8)));
    tag_variables["daystr"] = splitString(FrameOfReference, 10, 0, 8);
    tag_variables["timestr"] = splitString(FrameOfReference, 10, 8, 6) + "." + splitString(FrameOfReference, 10, 14, 3);

//    ImplementationClassUID = FrameOfReference(1:(ind(7)-1));

//    tMeasuredBaselineString=xprot{3}.x.MEAS.sProtConsistencyInfo.tMeasuredBaselineString;
//    ind = strfind(tMeasuredBaselineString,'_');
//    if (isempty(ind))
//        tMeasuredBaselineString=xprot{3}.x.MEAS.sProtConsistencyInfo.tBaselineString;
//    ind = strfind(tMeasuredBaselineString,'_');
//    end;
//    ind = strfind(tMeasuredBaselineString,'_');

    // "N4_VB20P_LATEST_20130629"
    std::string tMeasuredBaselineString;
    success &= getTagValue("XProtocol.**.MEAS.sProtConsistencyInfo.tMeasuredBaselineString", tMeasuredBaselineString, log_stream);
    if (tMeasuredBaselineString.find('_') == std::string::npos) {
        success &= getTagValue( "XProtocol.**.MEAS.sProtConsistencyInfo.tBaselineString", tMeasuredBaselineString, log_stream);
    }

//    IdeaVersion = tMeasuredBaselineString((ind(1)+1):(ind(2)-1));
    tag_variables["ImplementationVersionName"] = "MR_" + splitString<'_'>(tMeasuredBaselineString, 1);

//    %seriestime = datestr(clock,'HHMMSS.FFF000');
//    try
//        time_s = round(double(image_obj{1}.image.ulTimeStamp(1)*2.5/1000));
//        catch
//        time_s = round(double(image_obj{2}.image.ulTimeStamp(1)*2.5/1000));
//        end
//                seriestime = datestr(time_s/(24*60*60), 'HHMMSS.FFF000');

//        dateofseccapture = datestr(clock,'YYYYMMDD');
//        timeofseccapture = datestr(clock,'HHMMSS.FFF000');

    std::time_t now = std::time(nullptr);

    std::stringstream dateofseccapture;
    dateofseccapture << std::put_time(std::localtime(&now), "%Y%m%d");
    tag_variables["dateofseccapture"] = dateofseccapture.str();
    std::stringstream timeofseccapture;
    timeofseccapture << std::put_time(std::localtime(&now), "%H%M%S.000000");
    tag_variables["timeofseccapture"] = timeofseccapture.str();

//        try ExamMemoryUID = xprot{1}.x.PARC.SERVICES.ExamMemoryService.ExamMemoryUID; catch ExamMemoryUID = ''; end;
//            ind = strfind(ExamMemoryUID,'_');
//            if (~isempty(ind))
//                studytime = [ExamMemoryUID((ind(4)+1):(ind(5)-1)) '.' ExamMemoryUID((ind(5)+1):end) '000'];
//            else
//            studytime = '000000.000000';
//            end;
    std::string ExamMemoryUID;
    std::string studytime_default = "000000.000000";
    auto studytime = studytime_default;
    if (getTagValue("XProtocol.**.ExamMemoryService.ExamMemoryUID", ExamMemoryUID, log_stream)) {
        studytime = splitString<'_'>(ExamMemoryUID, 4) + "." + splitString<'_'>(ExamMemoryUID, 5) + "000";
    }
    tag_variables["studytime"] = studytime.size() == studytime_default.size() ? studytime : studytime_default;


    // selected from lines 72:244 of makeHeaders.m

//        common.RepetitionTime = xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.TR/1000;
    std::string RepetitionTime;
    success &= getTagValue({"XProtocol.**.FillMiniHeaderData.TR", "XProtocol.**.SpacingBetweenSlices.ParamDouble.TR"},
                           RepetitionTime, log_stream);
    try {
        tag_variables["RepetitionTime"] = std::to_string(stoi(RepetitionTime) / 1000);
    } catch (...)  {
        log_stream << "Cannot set RepetitionTime: " << RepetitionTime << std::endl;
        success = false;
    }

//        common.EchoTime = xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.TE.x/1000;
    std::string EchoTime;
    success &= getTagValue({"XProtocol.**.FillMiniHeaderData.TE.ParamDouble",
                            "XProtocol.**.sTXSPEC.alTE"}, EchoTime, log_stream);
    try {
        tag_variables["EchoTime"] = std::to_string(stod(EchoTime) / 1000);
    } catch (...)  {
        log_stream << "Cannot set EchoTime: " << EchoTime << std::endl;
        success = false;
    }

//        common.ImagingFrequency = xprot{2}.x.DICOM.lFrequency/1e6;StudyInstanceUID
    std::string ImagingFrequency;
    success &= getTagValue("XProtocol.**.DICOM.lFrequency", ImagingFrequency, log_stream);
    try {
        tag_variables["ImagingFrequency"] = std::to_string(stod(ImagingFrequency) / 1e6);
    } catch (...)  {
        log_stream << "Cannot set ImagingFrequency: " << ImagingFrequency << std::endl;
        success = false;
    }

//        common.PixelSpacing = [xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.RoFOV/xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BaseResolution;...
//        xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.PeFOV/xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BaseResolution];
    std::string RoFOV;
    success &= getTagValue("XProtocol.**.acquisition.FeedbackRoot.RoFOV", RoFOV, log_stream);
    std::string PeFOV;
    success &= getTagValue("XProtocol.**.acquisition.FeedbackRoot.PeFOV", PeFOV, log_stream);
    std::string BaseResolution;
    success &= getTagValue("XProtocol.**.FillMiniHeaderData.BaseResolution", BaseResolution, log_stream);
    try {
        tag_variables["PixelSpacing"] = std::to_string(stod(RoFOV) / stod(BaseResolution))
                +"\\" + std::to_string(stod(PeFOV) / stod(BaseResolution));
    } catch (...)  {
        log_stream << "Cannot set PixelSpacing (RoFOV: " << RoFOV << ", PeFOV: " << PeFOV
            << ", BaseResolution: " << BaseResolution << ")" << std::endl;
        success = false;
    }

//        common.PercentPhaseFieldOfView = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.PeFOV/xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.RoFOV*100;
    try {
        tag_variables["PercentPhaseFieldOfView"] = std::to_string(stod(PeFOV) / stod(RoFOV) * 100);
    } catch (...)  {
        log_stream << "Cannot set PercentPhaseFieldOfView (RoFOV: " << RoFOV << ", PeFOV: " << PeFOV << ")" << std::endl;
        success = false;
    }

//        common.PatientAge = [num2str(round(xprot{2}.x.DICOM.flPatientAge),'%03.0f') 'Y'];
    std::string PatientAge;
    success &= getTagValue("XProtocol.**.DICOM.flPatientAge", PatientAge, log_stream);
    try {
        tag_variables["PatientAge"] = str(boost::format("%03.0fY") % stod(PatientAge));
    } catch (...)  {
        log_stream << "Cannot set PatientAge: " << PatientAge << std::endl;
        success = false;
    }
//        if (xprot{1}.x.PARC.RECOMPOSE.PatientSex == 2)
//        common.PatientSex = 'M';
//        else
//        common.PatientSex = 'F';
//        end;
    std::string PatientSex;
    if (getTagValue("XProtocol.**.RECOMPOSE.PatientSex", PatientSex, log_stream)) {
        tag_variables["PatientSex"] = PatientSex == "1" ? "F" : PatientSex == "2" ? "M" : "O";
    } else {
        success = false;
    }



//        common.PixelBandwidth = round(1/2*1/(xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.RealDwellTime.x*1e-9*xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.NImageCols));
    std::string RealDwellTime;
    success &= getTagValue("XProtocol.**.RealDwellTime.ParamLong", RealDwellTime, log_stream);
    std::string NImageCols;
    success &= getTagValue("XProtocol.**.acquisition.FeedbackRoot.NImageCols", NImageCols, log_stream);
    try {
        tag_variables["PixelBandwidth"] = std::to_string(int(round(1e9 / (stod(RealDwellTime) * stod(NImageCols) * 2))));
    } catch (...)  {
        log_stream << "Cannot set PixelBandwidth (RealDwellTime: " << RealDwellTime
            << ", NImageCols " << NImageCols << ") " << std::endl;
        success = false;
    }

//        common.DeviceSerialNumber = xprot{3}.x.DICOM.DeviceSerialNumber;
//        common.StationName = ['MRC' common.DeviceSerialNumber];
    std::string StationName;
    success &= getTagValue("XProtocol.**.DICOM.DeviceSerialNumber", StationName, log_stream);
    tag_variables["StationName"] = "MRC" + StationName;

//        common.PerformedProcedureStepID = ['MR' daystr studytime(1:6)];
    tag_variables["PerformedProcedureStepID"] = std::string("MR") + tag_variables["daystr"] + tag_variables["studytime"].substr(0, 6);

//        try common.PatientName = xprot{1}.x.PARC.RECOMPOSE.PatientName; catch common.PatientName = ''; end;
//        try common.PatientName = xprot{1}.x.PARC.RECOMPOSE.tPatientName; catch  end;
    std::string PatientName;
    success &= getTagValue({"XProtocol.**.RECOMPOSE.PatientName", "XProtocol.**.RECOMPOSE.tPatientName"}, PatientName, log_stream);
    tag_variables["PatientName"] = PatientName;


    double PatientSize;
    success &= getTagValue("XProtocol.**.YAPS.flPatientHeight", PatientSize, log_stream);
    tag_variables["PatientSize"] = std::to_string(PatientSize/1000);

    success &= getTagValue("XProtocol.**.DICOM.tMRAcquisitionType", tag_variables["MRAcquisitionType"], log_stream);

    success &= getTagValue("XProtocol.**.FillMiniHeaderData.NoImagesPerSlab", tag_variables["NoImagesPerSlab"], log_stream);

    std::string ImageLines;
    success &= getTagValue({"XProtocol.**.DERIVED.ImageLines",
                            "XProtocol.**.FeedbackRoot.NImageLins"}, ImageLines, log_stream);
    std::string ImageColumns;
    success &= getTagValue({"XProtocol.**.DERIVED.ImageColumns",
                            "XProtocol.**.FeedbackRoot.NImageCols"}, ImageColumns, log_stream);
    tag_variables["AcquisitionMatrix"] = ImageLines +"\\0\\0\\"+ImageColumns;

    return success;
}


bool SyngoWrapper::read_mrprot(TagsLookupTable::mrprot_tags_t &mrprot_tags,
                               std::ostream &log_stream,
                               const std::string &begin_marker, const std::string &end_marker)
{
    try
    {
        std::ifstream::sync_with_stdio(false);
        std::basic_ifstream<std::string::value_type> stream(smprot.FileName());

        // shift to the last measuremnt
        stream.seekg(smprot.GetLastMeaseOffset());

        //advance the stream until begin_marker is found
        if (!begin_marker.empty()) {
            std::string found_str;
            while (!stream.eof()) {
                std::string::value_type ch;
                stream.get(ch);
                found_str += ch;
                if (begin_marker.find(found_str) == 0) {
                    // found_str is a substring of the begin_marker; let's check if it is full match
                    if (found_str.size() == begin_marker.size()) {
                        break;
                    }
                } else {
                    // remove characters one by one from the begining of found_str and check it every time against begin_marker
                    // btw, begin_marker.find("") evaluates to 0
                    while (begin_marker.find(found_str) != 0) {
                        found_str.erase(0, 1);
                    }
                }
            }
        }

        boost::regex line_regex("(?'key'\\S*)\\s*=\\s*\"?(?'val'\\S*)\"?");
        while (!stream.eof()) {

            std::string line;
            getline(stream, line);
            if (line.find(end_marker) != std::string::npos) {
                break;
            }

            boost::smatch what;
            if (boost::regex_search(line, what, line_regex)) {
                auto key = what["key"].str();
                auto val = what["val"].str();
                mrprot_tags[key] = val;
            }

        }
    } catch (const std::exception & e) {
        log_stream << "Error reading mprot from " << smprot.FileName() << ": " << e.what() << std::endl;
        return false;
    }

    return true;
}

