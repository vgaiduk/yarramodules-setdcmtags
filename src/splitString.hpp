//
// Created by Vadim on 10/26/15.
//

#ifndef SETDCMTAGS_SPLITSTRING_HPP
#define SETDCMTAGS_SPLITSTRING_HPP

template <char splitterChar = '.'>
std::string splitString(const std::string & s, size_t split_num, size_t pos = 0, size_t count = 0) {

    size_t split_pos = 0;
    for (size_t i = 0; i < split_num; ++i){
        split_pos = s.find(splitterChar, split_pos);
        if(split_pos == std::string::npos) {
            return "";
        }
        //skip the split character
        ++split_pos;
    }

    size_t next_dot = s.find(splitterChar, split_pos);
    if(next_dot == std::string::npos) {
        next_dot = s.size();
    }
    if (count == 0) {
        count = next_dot - split_pos - pos;
    }

    if (split_pos + pos + count > next_dot) {
        return "";
    }

    auto result = s.substr(split_pos +pos, count);
    if (result.size() != count) {
        //the calculated indexes went out of bounds of the orginal string
        return "";
    }

    return result;
}


#endif //SETDCMTAGS_SPLITSTRING_HPP
