
#include "syngoWrapper.h"

#include "dcmtk/config/osconfig.h"   // make sure OS specific configuration is included first
#include "dcmtk/dcmdata/cmdlnarg.h"
#include "dcmtk/ofstd/ofconapp.h"
#include "dcmtk/dcmdata/dcpath.h"
#include "dcmtk/dcmdata/dcerror.h"
#include "dcmtk/dcmdata/dctk.h"

#include "mdfdsman.h"
#include "dcmtk/ofstd/ofstd.h"
#include "dcmtk/dcmdata/dctk.h"
#include "dcmtk/dcmdata/dcistrmz.h"    /* for dcmZlibExpectRFC1950Encoding */

#include <boost/format.hpp>
#include <boost/regex.hpp>

#include <iostream>
#include <deque>
#include <boost/filesystem.hpp>

#define SHORTCOL 4
#define LONGCOL 21

#ifdef WITH_ZLIB
BEGIN_EXTERN_C
#include <zlib.h>
END_EXTERN_C
#endif

#define STR(s) XSTR(s)
#define XSTR(s) #s


static OFLogger setDcmtagLogger = OFLog::getLogger("dcmtk.apps." STR(PROJECT_NAME));


OFCondition loadRawFile(const OFString & rawDataFile,
                        TagsLookupTable & tagsLookupTable,
                        OFBool stop_on_error = true,
                        const OFString & writeXMLfname = "",
                        const OFString & writeINFOfname = "",
                        const OFString & writeINIfname = "",
                        const OFString & writeTXTfname = "") {

    OFLOG_INFO(setDcmtagLogger, "Processing file: " << rawDataFile);

    try {
        // Read and parse protocol from file
        SyngoWrapper syngoWrapper(rawDataFile.c_str());

        std::stringstream log_stream;
        if (!syngoWrapper.read_mrprot(tagsLookupTable.mrprot_tags, log_stream)) {
            OFLOG_WARN(setDcmtagLogger, "Error when reading mprot: " << log_stream.str());
            if (stop_on_error) {
                return makeOFCondition(OFM_dcmdata, 18, OF_error, "Error when reading mprot");
            }
        } else {
            OFLOG_INFO(setDcmtagLogger, "Reading mprot; " << log_stream.str());
        };

        try {
            if (!writeXMLfname.empty()) {
                OFLOG_INFO(setDcmtagLogger, "Writing raw data file tags to " << writeXMLfname);
                syngoWrapper.printSyngoTagsToXML(writeXMLfname.c_str());
            }

            if (!writeINFOfname.empty()) {
                OFLOG_INFO(setDcmtagLogger, "Writing raw data file tags to " << writeINFOfname);
                syngoWrapper.printSyngoTagsToINFO(writeINFOfname.c_str());
            }

            if (!writeINIfname.empty()) {
                OFLOG_INFO(setDcmtagLogger, "Writing raw data file tags to " << writeINIfname);
                syngoWrapper.printSyngoTagsToINI(writeINIfname.c_str());
            }

            if (!writeTXTfname.empty()) {
                OFLOG_INFO(setDcmtagLogger, "Writing raw data file tags to " << writeTXTfname);
                syngoWrapper.printSyngoTags(writeTXTfname.c_str());
            }
        } catch (const std::runtime_error & e) {
            OFLOG_WARN(setDcmtagLogger, "Error writing raw data file tags: " << e.what());
        }

        //do not forget to reset the log_stream
        log_stream.str("");
        if (!syngoWrapper.processDcmTags(tagsLookupTable.tag_entries, log_stream)) {
            OFLOG_WARN(setDcmtagLogger, "Error when processing DCM Tags: " << log_stream.str());
            if (stop_on_error) {
                return makeOFCondition(OFM_dcmdata, 18, OF_error, "Error when processing DCM Tags");
            }
        } else {
            OFLOG_INFO(setDcmtagLogger, "Processing DCM Tags; " << log_stream.str());
        };

        //do not forget to reset the log_stream
        log_stream.str("");
        if (!syngoWrapper.calcDcmtagVariables(tagsLookupTable.tag_variables, log_stream)) {
            OFLOG_WARN(setDcmtagLogger, "Error when processing DCM Tag variables: " << log_stream.str());
            if (stop_on_error) {
                return makeOFCondition(OFM_dcmdata, 18, OF_error, "Error when processing DCM Tag variables");
            }
        } else {
            OFLOG_INFO(setDcmtagLogger, "Processing DCM Tag variables; " << log_stream.str());
        };

        return EC_Normal;
    }
    catch (const SyngoMRProtocol::parse_exception& status) {
        switch (status) {
            case SyngoMRProtocol::FAILED_TO_FIND_XPROTOCOL:
                return makeOFCondition(OFM_dcmdata, 18, OF_error, "Could not find the XProtocol");
            case SyngoMRProtocol::HIT_EOF_BEFORE_FINISHING:
                return makeOFCondition(OFM_dcmdata, 18, OF_error, "Hit EOF before finishing parsing");
            case SyngoMRProtocol::PROTOCOL_HIERARCHY_ERROR:
                return makeOFCondition(OFM_dcmdata, 18, OF_error, "Protocol hierarchy error");
            default:
                assert(false); // We should have never get here
                break;
        }
    }
    catch (const SyngoMRProtocol::read_exception& status) {
        switch (status) {
            case SyngoMRProtocol::CANNOT_OPEN:
                return makeOFCondition(OFM_dcmdata, 18, OF_error, "Failed to open file");
            case SyngoMRProtocol::TOO_LARGE_HEADER:
                return makeOFCondition(OFM_dcmdata, 18, OF_error, "Header way too long");
            case SyngoMRProtocol::ZERO_HEADER_LENGTH:
                return makeOFCondition(OFM_dcmdata, 18, OF_error, "Zero header length");
            default:
                assert(false); // We should have never get here
                break;
        }
    }
    catch (const std::exception& e) {
        return makeOFCondition(OFM_dcmdata, 18, OF_error, e.what());
    }
}

OFCondition registerPrivateTags(MdfDatasetManager & ds_man, const char * tag_name, const char * tag_value)
{
    OFCondition result;
    std::string creator_tag;

    const DcmDictEntry *dcmDictEntry = dcmDataDict.rdlock().findEntry(tag_name);
    if (dcmDictEntry && dcmDictEntry->isPrivate()) {

        auto g = dcmDictEntry-> getKey().getGroup();
        auto e = dcmDictEntry-> getKey().getElement();

        e /= 0x100;
        if (e >= 0x10) {
            creator_tag = str(boost::format("(%04x,%04x)") % g % e);
        }
    }
    dcmDataDict.unlock();

    if (!creator_tag.empty()) {

        result = ds_man.modifyOrInsertPath(creator_tag.c_str(), dcmDictEntry->getPrivateCreator(), OFFalse);
        if (result.bad()) {
            return result;
        }
    }

    result = ds_man.modifyOrInsertPath(tag_name, tag_value, OFFalse);
    return result;
}

OFCondition loadAndWriteDicomFile(TagsLookupTable & tagsLookupTable,
                                  const char * inFilename,
                                  const char * outFilename = nullptr,
                                  OFBool stop_on_error = false,
                                  OFBool log_the_tags_and_vars = true)
{
    OFCondition result;
    // free memory
    MdfDatasetManager ds_man;
//    ds_man.setModifyUNValues(!ignore_un_modifies);
    OFLOG_INFO(setDcmtagLogger, "Processing file: " << inFilename);
    // load file into dataset manager
    result = ds_man.loadFile(inFilename);
    if (result.bad()) {
        return result;
    }

    for (auto & entry : tagsLookupTable.tag_entries) {

        if (!entry.dcmtag.empty() && entry.variable != TagsLookupTable::variableKeep
            && (entry.sequence.empty() || entry.sequence == tagsLookupTable.tag_variables[TagsLookupTable::variableSeries])
            && (entry.slice.empty() || entry.slice == tagsLookupTable.tag_variables[TagsLookupTable::variableSlice]))
        {

            if (entry.variable == TagsLookupTable::variableUID) {

                const DcmDictEntry *dcmDictEntry = dcmDataDict.rdlock().findEntry(entry.dcmtag.c_str());
                if (dcmDictEntry) {
                   result = ds_man.generateAndInsertUID(*dcmDictEntry);
                    dcmDataDict.unlock();
                } else {
                    dcmDataDict.unlock();
                    OFLOG_WARN(setDcmtagLogger, "Error creating DICOM UID tag [" << entry.dcmtag.c_str() << "]: no dictionary entry found");
                    if (stop_on_error) {
                        return result;
                    }
                }

            } else if (entry.variable == TagsLookupTable::variableSeriesUID) {

                std::string series_id;
                try {
                    series_id = tagsLookupTable.tag_variables.at(TagsLookupTable::variableSeries);
                }
                    // Leave series_id empty if the variable is not set
                catch (...) {};

                auto series_uid = tagsLookupTable.series_uids[series_id];

                if (series_uid.empty()) {
                    char uid[100];
                    dcmGenerateUniqueIdentifier(uid, SITE_SERIES_UID_ROOT);
                    series_uid = uid;
                    tagsLookupTable.series_uids[series_id] = series_uid;
                }
                result = ds_man.modifyOrInsertPath(entry.dcmtag.c_str(), series_uid.c_str(), OFFalse);

            } else if (entry.variable == TagsLookupTable::variableStudyUID) {

                if (tagsLookupTable.study_uid.empty()) {
                    char uid[100];
                    dcmGenerateUniqueIdentifier(uid, SITE_STUDY_UID_ROOT);
                    tagsLookupTable.study_uid = uid;
                }
                result = ds_man.modifyOrInsertPath(entry.dcmtag.c_str(), tagsLookupTable.study_uid.c_str(), OFFalse);

            } else {

//                result = ds_man.modifyOrInsertPath(entry.dcmtag.c_str(), entry.value.c_str(), OFFalse);
                result = registerPrivateTags(ds_man, entry.dcmtag.c_str(), entry.value.c_str());
            }
            if (result.bad()) {
                OFLOG_WARN(setDcmtagLogger, "Error writing DICOM tag [" << entry.dcmtag.c_str() << "]: " << result.text());
                if (stop_on_error) {
                    return result;
                }
            }
        }
    }

    if (log_the_tags_and_vars) {
        std::string logFileName(outFilename ? outFilename : inFilename);
        logFileName += ".log";

        std::basic_ofstream<boost::property_tree::ptree::key_type::value_type> log_stream(logFileName);

        log_stream << "Available DCM tags: " << std::endl;
        tagsLookupTable.printDcmTags(log_stream);

        log_stream << std::endl << "Available tag variables: " << std::endl;
        tagsLookupTable.printTagVariables(log_stream);

        log_stream << std::endl << "Available mprot tags: " << std::endl;
        tagsLookupTable.printMRprotTags(log_stream);
    }

    if (outFilename) {
        OFLOG_INFO(setDcmtagLogger, "Saving file: " << outFilename);
        result = ds_man.saveFile(outFilename);
    } else {
        OFLOG_INFO(setDcmtagLogger, "Saving file: " << inFilename);
        result = ds_man.saveFile();
    }

    return result;
}


int main(int argc, char *argv[])
{
    char rcsid[200];
    // print application header
    sprintf(rcsid, "$%s v%s, dcmtk v%s %s; build %s %s$", STR(PROJECT_NAME), STR(PROJECT_VERSION),
        OFFIS_DCMTK_VERSION, OFFIS_DCMTK_RELEASEDATE, __DATE__, __TIME__);

    /// helper class for console applications
    OFConsoleApplication app(STR(PROJECT_NAME), "Set DICOM tags to values from Siemens raw data file", rcsid);

    /// helper class for commandline parsing
    OFCommandLine cmd;

#ifndef DEBUG
    const int optionFlags = OFCommandLine::AF_NoWarning;
#else
    const int optionFlags = 0;
#endif

    cmd.setOptionColumns(LONGCOL, SHORTCOL);
    cmd.setParamColumn(LONGCOL + SHORTCOL + 4);

//    cmd.addParam("raw-file",    "raw .dat file name to read tag values from", OFCmdParam::PM_Mandatory);
    cmd.addParam("in-dcmfile",  "DICOM input file or directory name", OFCmdParam::PM_Optional);
    cmd.addParam("out-dcmfile", "DICOM output file or directory name to write to; if empty, the tags are written to the in-dcmfile", OFCmdParam::PM_Optional);

    // add options to commandline application
    cmd.addGroup("general options:", LONGCOL, SHORTCOL + 2);
    cmd.addOption("--help",                    "-h",      "print this help text and exit", OFCommandLine::AF_Exclusive);
    cmd.addOption("--version",                            "print version information and exit", OFCommandLine::AF_Exclusive);
    OFLog::addOptions(cmd);

    cmd.addGroup("processing options:");
        cmd.addOption("--raw-file",         "-r", 1, "[f]ilename: string", "raw .dat file name to read tag values from");
        cmd.addOption("--acc-number",       "-n", 1, "number", "ACC number", optionFlags);
        cmd.addOption("--stop-on-error",    "-e",    "Stop processing on error; default is report an error and continue", optionFlags);
        cmd.addOption("--dcmfile-regex",    "-x", 1, "[r]egex: string",  "regex used for parsing dcm file name to define "
                "series (capture name: 'series') and slice (capture name: 'slice'); "
                "default is  \"(?'name'\\\\w*)\\\\.(?'series'\\\\d*)\\\\.(?'slice'\\\\d*).dcm\"", optionFlags);
        cmd.addOption("--dict-file",        "-m", 1, "[f]ilename: string", "additional DICOM dictionary file");

        cmd.addOption("--use-ini",          "-i", 1, "[f]ilename: string", "use ini file to load tag lookup table", optionFlags);
//        cmd.addOption("--ini-overwrite",    "-o",    "overwrite tag lookup table instead of appending values from ini file", optionFlags);
        cmd.addOption("--additional-ini",   "-a", 1, "[f]ilename: string", "additional ini file (e.g. located in the data directory)"
                " to load tag lookup table; can overwrite the values loaded by -i option", optionFlags);
        cmd.addOption("--log-tag-values",  "-l", "log tag and varialbe values to the file <out_file_name>.dcm.log", optionFlags);

    cmd.addGroup("raw file tags dump options:");
        cmd.addOption("--write-xml",       "-wx", 0, "[f]ilename: string", "write tag dump in XML format; default: <datfile>.xml", optionFlags);
        cmd.addOption("--write-info",      "-wi", 0, "[f]ilename: string", "write tag dump in INFO format; default: <datfile>.info", optionFlags);
        cmd.addOption("--write-ini",       "-wn", 0, "[f]ilename: string", "write tag dump in INI format; default: <datfile>.ini", optionFlags);
        cmd.addOption("--write-txt",       "-wt", 0, "[f]ilename: string", "write tag dump in TXT format; default: <datfile>.txt", optionFlags);

    // evaluate commandline
    prepareCmdLineArgs(argc, argv, STR(PROJECT_NAME));
    if (app.parseCommandLine(cmd, argc, argv))
    {
        /* print help text and exit */
        if (cmd.getArgCount() == 0)
            app.printUsage();

        /* check exclusive options first */
        if (cmd.hasExclusiveOption())
        {
            if (cmd.findOption("--version"))
            {
                app.printHeader(OFTrue /*print host identifier*/);
                ofConsole.lockCout() << OFendl << "External libraries used:";
#ifdef WITH_ZLIB
                ofConsole.getCout() << OFendl << "- ZLIB, Version " << zlibVersion() << OFendl;
#else
                ofConsole.getCout() << " none" << OFendl;
#endif
                ofConsole.unlockCout();
                exit(0);
            }
        }

        OFLog::configureFromCommandLine(cmd, app);

        // make sure data dictionary is loaded
        if (!dcmDataDict.isDictionaryLoaded())
            OFLOG_WARN(setDcmtagLogger, "no data dictionary loaded, "
                                       << "check environment variable: " << DCM_DICT_ENVIRONMENT_VARIABLE);


        OFString rawDataFile;
        if (!cmd.findOption("--raw-file") || cmd.getValue(rawDataFile) != OFCommandLine::VS_Normal) {
            OFLOG_ERROR(setDcmtagLogger, "no raw file given!");
            return 1;
        }

        OFString accNumber;
        if (cmd.findOption("--acc-number"))
        {
            if (cmd.getValue(accNumber) != OFCommandLine::VS_Normal) {
                OFLOG_ERROR(setDcmtagLogger, "no ACC number given!");
            }
        }

        auto stop_on_error = cmd.findOption("--stop-on-error");

        auto log_the_tags_and_vars = cmd.findOption("--log-tag-values");

        OFString writeXMLfname;
        if (cmd.findOption("--write-xml"))
        {
            if (cmd.getValue(writeXMLfname) != OFCommandLine::VS_Normal) {
                writeXMLfname = rawDataFile + ".xml";
            }
        }

        OFString writeINFOfname;
        if (cmd.findOption("--write-info"))
        {
            if (cmd.getValue(writeINFOfname) != OFCommandLine::VS_Normal) {
                writeINFOfname = rawDataFile + ".info";
            }
        }

        OFString writeINIfname;
        if (cmd.findOption("--write-ini"))
        {
            if (cmd.getValue(writeINIfname) != OFCommandLine::VS_Normal) {
                writeINIfname = rawDataFile + ".ini";
            }
        }

        OFString writeTXTfname;
        if (cmd.findOption("--write-txt"))
        {
            if (cmd.getValue(writeTXTfname) != OFCommandLine::VS_Normal) {
                writeTXTfname = rawDataFile + ".txt";
            }
        }

        TagsLookupTable tagsLookupTable(accNumber.c_str());

        if (cmd.findOption("--use-ini"))
        {
            OFString iniFileName;
            if (cmd.getValue(iniFileName) == OFCommandLine::VS_Normal) {
                try {
                    tagsLookupTable.loadFromINI(iniFileName.c_str(), cmd.findOption("--ini-overwrite"));
                } catch (const std::runtime_error & e) {
                    OFLOG_FATAL(setDcmtagLogger, "Error reading ini file '" << iniFileName << "': " << e.what());
                    return 1;
                }
            } else {
                OFLOG_FATAL(setDcmtagLogger, "Error reading Tag Lookup Table from ini file");
                return 1;
            }
        }


        if (cmd.findOption("--additional-ini"))
        {
            OFString iniFileName;
            if (cmd.getValue(iniFileName) == OFCommandLine::VS_Normal) {
                try {
                    tagsLookupTable.loadFromINI(iniFileName.c_str());
                } catch (const std::runtime_error & e) {
                    OFLOG_FATAL(setDcmtagLogger, "Error reading ini file '" << iniFileName << "': " << e.what());
                    return 1;
                }
            } else {
                OFLOG_FATAL(setDcmtagLogger, "Error reading Tag Lookup Table from ini file");
                return 1;
            }
        }

        if (cmd.findOption("--dict-file"))
        {
            OFString dictFileName;
            DcmDataDictionary& globalDataDict = dcmDataDict.wrlock();

            if (cmd.getValue(dictFileName) != OFCommandLine::VS_Normal
                || !globalDataDict.loadDictionary(dictFileName.c_str()))
            {
                OFLOG_FATAL(setDcmtagLogger, "Error reading dictionary file "<< dictFileName);
                dcmDataDict.unlock();
                return 1;
            }
            dcmDataDict.unlock();
        }

        OFCondition cond = loadRawFile(rawDataFile, tagsLookupTable, stop_on_error, writeXMLfname, writeINFOfname, writeINIfname, writeTXTfname);
        if (cond.bad())
        {
            OFLOG_FATAL(setDcmtagLogger, "Error reading raw file: " << cond.text());
            return 1;
        }

        OFString inDicomFile;
        cmd.getParam(1, inDicomFile);
        if (inDicomFile.empty())
        {
            OFLOG_WARN(setDcmtagLogger, "no dicom file given!");
            return 0;
        }

        OFString outDicomFile;
        cmd.getParam(2, outDicomFile);

//        const char * filename_regex = "mrf_recon_contrast(?'series'\\d*)_slice(?'slice'\\d*).dcm";

        OFString filename_regex(TagsLookupTable::default_dcmfile_regex);
        if (cmd.findOption("--dcmfile-regex"))
        {
            if (!cmd.getValue(filename_regex) == OFCommandLine::VS_Normal) {
                OFLOG_FATAL(setDcmtagLogger, "Error getting dcmfile-regex");
                return 1;
            }
        }

        boost::filesystem::path in_dicom_path(inDicomFile.c_str());
        boost::filesystem::path out_dicom_path(outDicomFile.c_str());
        int dicomFileCounter = 0;
        try
        {
            if (boost::filesystem::exists(in_dicom_path))
            {
                if (boost::filesystem::is_regular_file(in_dicom_path)) {
                    std::stringstream log_stream;
                    std::string dcm_file = in_dicom_path.filename().string();
                    if (!tagsLookupTable.parseFilename(log_stream, dcm_file, filename_regex.c_str())) {
                        OFLOG_WARN(setDcmtagLogger, "Error when processing file " << dcm_file << std::endl << log_stream.str());
                    }

                    std::string out_file = (out_dicom_path.filename() != ".") ?  outDicomFile.c_str()
                        : (out_dicom_path.parent_path() / in_dicom_path.filename()).string();
                    cond = loadAndWriteDicomFile(tagsLookupTable, in_dicom_path.c_str(),
                                                 outDicomFile.empty() ? nullptr : out_file.c_str(), stop_on_error);
                    if (cond.bad()) {
                        OFLOG_FATAL(setDcmtagLogger, "Error writing DICOM file: " << cond.text());
                        return 1;
                    }
                    ++dicomFileCounter;
                }
                else if (boost::filesystem::is_directory(in_dicom_path))
                {
                    if (!outDicomFile.empty() && !boost::filesystem::is_directory(out_dicom_path)) {
                        OFLOG_FATAL(setDcmtagLogger, "The ouput directory for DICOM files must exists: " << outDicomFile);
                        return 1;
                    }
                    for (const auto &dir_entry : boost::filesystem::directory_iterator(in_dicom_path)) {
                        if (dir_entry.path().extension() == ".dcm") {
                            std::stringstream log_stream;
                            std::string dcm_file = dir_entry.path().filename().string();
                            if (!tagsLookupTable.parseFilename(log_stream, dcm_file, filename_regex.c_str())) {
                                OFLOG_WARN(setDcmtagLogger, "Error when processing file " << dcm_file << std::endl << log_stream.str());
                            }

                            std::string out_file = (out_dicom_path.parent_path() / dir_entry.path().filename()).string();
                            cond = loadAndWriteDicomFile(tagsLookupTable, dir_entry.path().c_str(),
                                                    outDicomFile.empty() ? nullptr : out_file.c_str(),
                                                    stop_on_error, log_the_tags_and_vars);
                            if (cond.bad()) {
                                OFLOG_FATAL(setDcmtagLogger, "Error writing DICOM file: " << cond.text());
                                return 1;
                            }
                            ++dicomFileCounter;
                        }
                    }
                } else {
                        OFLOG_FATAL(setDcmtagLogger, "Error writing DICOM file: " << inDicomFile << " exists, but is not a regular file or directory");
                        return 1;
                }
            }
            else {
                OFLOG_FATAL(setDcmtagLogger, "Error writing DICOM file: " << inDicomFile << " does not exist");
                return 1;
            }

            std::cout << STR(PROJECT_NAME) << " v." << STR(PROJECT_VERSION)
                << " done writting "  << dicomFileCounter << " files in " << tagsLookupTable.series_uids.size() << " series" << std::endl;
        }

        catch (const boost::filesystem::filesystem_error& ex)
        {
            OFLOG_FATAL(setDcmtagLogger, "Error writing DICOM file: " << ex.what());
            return 1;
        }

    }
    /* print resource identifier */
    OFLOG_DEBUG(setDcmtagLogger, rcsid << OFendl);

    return 0;
}


