//
// Created by Vadim on 10/20/15.
//

#ifndef SETDCMTAGS_DCMTAGS_HPP
#define SETDCMTAGS_DCMTAGS_HPP


#include "TagsLookupTable.h"

TagsLookupTable::dcmtag_entry_t dcmtags[] = {
        {"", "", ""},

// Lines 72:244 of makeHeaders.m

//        % the tags

//        common.Width = uint16(xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.NImageLins);
//        {"Width", "XProtocol.Config.ParamMap.PARC.DERIVED.sGroupArray.PIPE.acquisition.FeedbackRoot.NImageLins"},
//        {"Width", "XProtocol.Config.ParamMap.PARC.PIPE.acquisition.FeedbackRoot.NImageLins"},

//        common.Height = uint16(xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.NImageCols);
//        {"Height", "XProtocol.Config.ParamMap.PARC.DERIVED.sGroupArray.PIPE.acquisition.FeedbackRoot.NImageCols"},
//        {"Height", "XProtocol.Config.ParamMap.PARC.PIPE.acquisition.FeedbackRoot.NImageCols"},

//        %common.BitDepth = 12;%log2(str2num(xprot{1}.x.PARC.HARDWARE.MaxMemory));

//        common.ColorType = 'grayscale';
//        {"ColorType", "", "grayscale"},

//        common.MediaStorageSOPClassUID = '1.2.840.10008.5.1.4.1.1.4';
//        {"MediaStorageSOPClassUID", "", "1.2.840.10008.5.1.4.1.1.4", ""},

//        common.TransferSyntaxUID = '1.2.840.10008.1.2.1';
//        {"TransferSyntaxUID", "", "1.2.840.10008.5.1.4.1.1.4", ""},

//        common.ImplementationClassUID = ImplementationClassUID;
//        {"ImplementationClassUID", "", "", "uid"},

//        common.ImplementationVersionName = ['MR_' IdeaVersion];
//        {"ImplementationVersionName", "", "", "ImplementationVersionName"},

//        common.SpecificCharacterSet = 'ISO_IR 100';
        {"SpecificCharacterSet", "", "ISO_IR 100"},

//        common.ImageType = 'ORIGINAL\PRIMARY\M\1\NORM';
        {"ImageType", "", "ORIGINAL\\PRIMARY\\M\\1\\NORM"},

//        common.InstanceCreationDate = daystr;
        {"InstanceCreationDate", "", "", "daystr"},

//        common.InstanceCreationTime = timeofseccapture;
        {"InstanceCreationTime", "", "", "timestr"},

//        common.SOPClassUID = common.MediaStorageSOPClassUID;
//        {"SOPClassUID", "", "", "uid"},

//        common.StudyDate = daystr;
        {"StudyDate", "", "", "daystr"},

//        common.SeriesDate = daystr;
        {"SeriesDate", "", "", "daystr"},

//        common.AcquisitionDate = daystr;
        {"AcquisitionDate", "", "", "daystr"},

//        common.ContentDate = dateofseccapture;
        {"ContentDate", "", "", "dateofseccapture"},

//        common.StudyTime = studytime;
        {"StudyTime", "", "", "studytime"},

//        common.SeriesTime = studytime;
        {"SeriesTime", "", "", "studytime"},

//        common.AcquisitionTime = studytime;
        {"AcquisitionTime", "", "", "studytime"},

//        common.ContentTime = timeofseccapture;
        {"ContentTime", "", "", "timeofseccapture"},

//        common.PatientID = xprot{1}.x.PARC.RECOMPOSE.PatientID;
        {"PatientID", "XProtocol.**.RECOMPOSE.PatientID"},

//        common.PatientBirthDate = xprot{1}.x.PARC.RECOMPOSE.PatientBirthDay;
        {"PatientBirthDate", "XProtocol.**.RECOMPOSE.PatientBirthDay"},

//        if (strcmp(common.PatientBirthDate,'xxxxxxxx'))
//        common.PatientBirthDate = '00000000';
//        end;

//        if (xprot{1}.x.PARC.RECOMPOSE.PatientSex == 2)
//        common.PatientSex = 'M';
//        else
//        common.PatientSex = 'F';
//        end;
        {"PatientSex", "", "", "PatientSex"},

//        try common.PatientName = xprot{1}.x.PARC.RECOMPOSE.PatientName; catch common.PatientName = ''; end;
//        try common.PatientName = xprot{1}.x.PARC.RECOMPOSE.tPatientName; catch  end;
        {"PatientName", "", "", "PatientName"},

//        common.PatientAge = [num2str(round(xprot{2}.x.DICOM.flPatientAge),'%03.0f') 'Y'];
        {"PatientAge", "", "", "PatientAge"},

//        common.PatientWeight = xprot{2}.x.DICOM.flUsedPatientWeight;
        {"PatientWeight", "XProtocol.**.DICOM.flUsedPatientWeight"},

//        common.Modality = xprot{2}.x.DICOM.Modality; % 'MR'
        {"Modality", "XProtocol.**.DICOM.Modality"},

//        common.Manufacturer = xprot{3}.x.DICOM.Manufacturer;
        {"Manufacturer", "XProtocol.**.DICOM.Manufacturer"},

//        common.ManufacturerModelName = xprot{3}.x.DICOM.ManufacturersModelName;
        {"ManufacturerModelName", "XProtocol.**.DICOM.ManufacturersModelName"},

//        common.SoftwareVersion = xprot{3}.x.DICOM.SoftwareVersions;
        {"SoftwareVersions", "XProtocol.**.DICOM.SoftwareVersions"},

//        common.InstitutionName = xprot{3}.x.DICOM.InstitutionName;
        {"InstitutionName", "XProtocol.**.DICOM.InstitutionName"},

//        common.InstitutionAddress = xprot{3}.x.DICOM.InstitutionAddress;
        {"InstitutionAddress", "XProtocol.**.DICOM.InstitutionAddress"},

//        common.DeviceSerialNumber = xprot{3}.x.DICOM.DeviceSerialNumber;
        {"DeviceSerialNumber", "XProtocol.**.DICOM.DeviceSerialNumber"},

//        common.StationName = ['MRC' common.DeviceSerialNumber];
        {"StationName", "", "", "StationName"},

//        common.SeriesDescription = xprot{1}.x.PARC.SERVICES.RAIDLocal.ProtocolName;
        {"SeriesDescription", "XProtocol.**.DICOM.tProtocolName"},

//        common.PerformingPhysicianName.FamilyName = xprot{2}.x.DICOM.tPerfPhysiciansName;
        {"PerformingPhysicianName", "XProtocol.**.DICOM.tPerfPhysiciansName"},

/*
        {"PerformingPhysicianName.FamilyName", "XProtocol.**.DICOM.tPerfPhysiciansName"},

//        common.PerformingPhysicianName.GivenName = '';
        {"PerformingPhysicianName.GivenName", "", ""},

//        common.PerformingPhysicianName.MiddleName = '';
        {"PerformingPhysicianName.MiddleName", "", ""},

//        common.PerformingPhysicianName.NamePrefix = '';
        {"PerformingPhysicianName.NamePrefix", "", ""},

//        common.PerformingPhysicianName.NameSuffix = '';
        {"PerformingPhysicianName.NameSuffix", "", ""},
*/

//        common.InstitutionalDepartmentName = 'Department';
        {"InstitutionalDepartmentName", "", "Department"},

//        for i = 1:3
//        eval(['common.ReferencedImageSequence.Item_' num2str(i) '.ReferencedSOPClassUID = common.MediaStorageSOPClassUID;']);
//        eval(['common.ReferencedImageSequence.Item_' num2str(i) '.ReferencedSOPInstanceUID = xprot{3}.x.MEAS.tReferenceImage' num2str(i-1) ';']);
//        end;

//        common.BodyPartExamined = xprot{2}.x.DICOM.tBodyPartExamined;
        {"BodyPartExamined", "XProtocol.**.DICOM.tBodyPartExamined", ""},

//        common.tScanningSequence = xprot{2}.x.DICOM.tScanningSequence;
        {"ScanningSequence", "XProtocol.**.DICOM.tScanningSequence", ""},

//        common.SequenceVariant = xprot{2}.x.DICOM.tSequenceVariant;
        {"SequenceVariant", "XProtocol.**.DICOM.tSequenceVariant", ""},

//        common.ScanOptions = xprot{2}.x.DICOM.tScanOptions;
        {"ScanOptions", "XProtocol.**.DICOM.tScanOptions", ""},

//        common.MRAcquisitionType = xprot{2}.x.DICOM.tMRAcquisitionType;
        {"MRAcquisitionType", "XProtocol.**.DICOM.tMRAcquisitionType", ""},

//        common.SequenceName = xprot{3}.x.YAPS.tSequenceString;
        {"SequenceName", {"XProtocol.**.YAPS.tSequenceString",
                          "XProtocol.**.FillMiniHeaderData.SequenceString",
                          "XProtocol.**.source.SequenceString",
                         }, ""},

//        common.AngioFlag = xprot{3}.x.SPICE.PIPE.Receiver.source.AngioFlag;
        {"AngioFlag", "XProtocol.**.Receiver.source.AngioFlag"},

//        common.NumberOfAverages = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.NAveMeas;
        {"NumberOfAverages", "XProtocol.**.acquisition.FeedbackRoot.NAveMeas", ""},

//        common.RepetitionTime = xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.TR/1000;
        {"RepetitionTime", "", "", "RepetitionTime"},

//        common.EchoTime = xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.TE.x/1000;
        {"EchoTime", "", "", "EchoTime"},

//        common.ImagingFrequency = xprot{2}.x.DICOM.lFrequency/1e6;
        {"ImagingFrequency", "", "", "ImagingFrequency"},

//        common.ImagedNucleus = xprot{3}.x.SPICE.PIPE.Receiver.source.ResonantNucleus;
        {"ImagedNucleus", "XProtocol.**.Receiver.source.ResonantNucleus"},

//        common.MagneticFieldStrength = round(xprot{2}.x.DICOM.flMagneticFieldStrength);
        {"MagneticFieldStrength", "XProtocol.**.DICOM.flMagneticFieldStrength", ""},

//    nechoes = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.NEcoMeas;
//        common.EchoTrainLength = nechoes;
        {"EchoTrainLength", "XProtocol.**.acquisition.FeedbackRoot.NEcoMeas"},

//        common.SpacingBetweenSlices = xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.SpacingBetweenSlices.x;
        {"SpacingBetweenSlices", "XProtocol.**.FillMiniHeaderData.SpacingBetweenSlices.ParamDouble", ""},

//        common.SliceThickness = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.SliceThickness.x;
//        {"SliceThickness", "XProtocol.Config.ParamMap.PARC.PIPE.acquisition.FeedbackRoot.SliceThickness.ParamDouble", ""},
        {"SliceThickness", "", "", "SliceThickness"},

//        common.NumberOfPhaseEncodingSteps = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.NLinMeas;
        {"NumberOfPhaseEncodingSteps", "XProtocol.**.acquisition.FeedbackRoot.NLinMeas", ""},

//        common.PercentPhaseFieldOfView = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.PeFOV/xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.RoFOV*100;
        {"PercentPhaseFieldOfView", "", "", "PercentPhaseFieldOfView"},

//        common.PixelBandwidth = round(1/2*1/(xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.RealDwellTime.x*1e-9*xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.NImageCols));
        {"PixelBandwidth", "", "", "PixelBandwidth"},

//        try
//        common.FlipAngle = mrprot{8}.x.PARC.PIPE.Displayer.display.FlipAngle;
//        catch
//        common.FlipAngle = xprot{3}.x.SPICE.PIPE.Displayer.display.FlipAngle;
//        end;
        {"FlipAngle", {"XProtocol.**.display.FlipAngle",
                       "XProtocol.**.DICOM.adFlipAngleDegree", ""}, "", "" },

//        common.ProtocolName = xprot{1}.x.PARC.SERVICES.RAIDLocal.ProtocolName;
        {"ProtocolName", "XProtocol.**.DICOM.tProtocolName", ""},

//        common.DateOfLastCalibration = strrep(xprot{3}.x.YAPS.atTXCalibDate,'" "','\');
//        % common.DateOfLastCalibration = strrep(xprot{3}.x.YAPS.atTXCalibDate,'" "');
//        common.TimeOfLastCalibration = [strrep(xprot{3}.x.YAPS.atTXCalibTime,'" "','.000000\') '.000000'];
//        common.TransmitCoilName = xprot{2}.x.DICOM.TransmittingCoil;
        {"TransmitCoilName", "XProtocol.**.DICOM.TransmittingCoil", ""},

//        common.PatientPosition = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.PatientPosition;
        {"PatientPosition", "XProtocol.**.DICOM.tPatientPosition", ""},

//        common.Private_0019_10xx_Creator = 'SIEMENS MR HEADER';
//        common.Private_0019_1008 = 'IMAGE NUM 4';
        {"0019ImageNum4", "", "IMAGE NUM 4"},

//        common.Private_0051_10xx_Creator = 'SIEMENS MR HEADER';

//        common.Private_0051_1008 = 'IMAGE NUM 4';
        {"0051ImageNum4", "", "IMAGE NUM 4"},

//        common.Private_0029_10xx_Creator = 'SIEMENS CSA HEADER';

//        common.Private_0029_11xx_Creator = 'SIEMENS MEDCOM HEADER2';

//        common.CSAImageHeaderType = 'IMAGE NUM 4';
        {"CSAImageHeaderType", "", "IMAGE NUM 4"},

//        common.CSASeriesHeaderType = 'MR';
        {"CSASeriesHeaderType", "", "MR"},

//        common.CSAImageHeaderVersion = daystr;
        {"CSAImageHeaderVersion", "", "", "daystr"},

//        common.CSASeriesHeaderVersion = daystr;
        {"CSASeriesHeaderVersion", "", "", "daystr"},

//        common.PerformedProcedureStepStartDate = daystr;
        {"PerformedProcedureStepStartDate", "", "", "daystr"},

//        common.FrameOfReferenceUID = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.FrameOfReference;
        {"FrameOfReferenceUID", "XProtocol.**.acquisition.FeedbackRoot.FrameOfReference", ""},

//        common.Rows = xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BaseResolution;
//        {"Rows", "XProtocol.Config.ParamMap.PARC.SERVICES.FillMiniHeaderData.BaseResolution", ""},
//        {"Rows", "XProtocol.Config.ParamMap.PARC.DERIVED.ImageLines", ""},

//        common.Columns = xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.PhaseEncodingLines;
//        {"Columns", "XProtocol.Config.ParamMap.PARC.SERVICES.FillMiniHeaderData.PhaseEncodingLines", ""},
//        {"Columns", "XProtocol.Config.ParamMap.PARC.SERVICES.FillMiniHeaderData.BaseResolution", ""},
//        {"Columns", "XProtocol.Config.ParamMap.PARC.DERIVED.ImageColumns", ""},


//        % common.PixelSpacing = [xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.RoFOV/xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BaseResolution;...
//        %                       xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.PeFOV/xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.PhaseEncodingLines];
//
//        % Adjust for radial
//        common.PixelSpacing = [xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.RoFOV/xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BaseResolution;...
//        xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.PeFOV/xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BaseResolution];
        {"PixelSpacing", "", "", "PixelSpacing"},


//        try common.BitsStored = common.BitDepth; catch end;
//        try common.HighBit = common.BitDepth-1; catch end;

//        common.PerformedProcedureStepStartTime = studytime;
        {"PerformedProcedureStepStartTime", "", "", "studytime"},

//        common.PerformedProcedureStepID = ['MR' daystr studytime(1:6)];
        {"PerformedProcedureStepID", "", "", "PerformedProcedureStepID"},

//
//        common.Private_0019_1012 = int32([0 0 0]);
//        xcoord = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.SBCSOriginPositionX;
//        ycoord = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.SBCSOriginPositionY;
//        zcoord = xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.SBCSOriginPositionZ;
//        if (~isempty(xcoord)) common.Private_0019_1012(1) = xcoord; end;
//        if (~isempty(ycoord)) common.Private_0019_1012(2) = ycoord; end;
//        if (~isempty(zcoord)) common.Private_0019_1012(3) = zcoord; end;
//
//        common.Private_0019_1013 = int32(common.Private_0019_1012);
//        try common.Private_0019_1018 = xprot{1}.x.PARC.PIPE.SystemFunctors_ps.ChannelMixingFunctor.RawDwellTime.x; catch end;
//        common.Private_0019_1028 = xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BandwidthPerPixelPhaseEncode;
//
//        common.Private_0051_1017 = ['SL ' num2str(common.SliceThickness,'%3.1f')];
//
//        % common.Private_0051_100b = [num2str(xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BaseResolution) 'p*' ...
//        %                             num2str(xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.PhaseEncodingLines)];
//
//        % Adjustment for radial
//        common.Private_0051_100b = [num2str(xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BaseResolution) 'p*' ...
//        num2str(xprot{1}.x.PARC.SERVICES.FillMiniHeaderData.BaseResolution)];
//
//        common.Private_0051_100c = ['FoV ' num2str(xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.RoFOV) ...
//        '*' num2str(xprot{1}.x.PARC.PIPE.acquisition.FeedbackRoot.PeFOV)];
//        try common.Private_0051_100f = mrprot{1}.sCoilSelectMeas.sCoilStringForConversion; catch end;
//        common.Private_0051_1019 = ['  /' strrep(xprot{2}.x.DICOM.tScanOptions,'\','/')];
//        ind = strfind(common.ImageType,'\');
//        common.Private_0051_1016 = strrep(common.ImageType((ind(2)+1):end),'\','/');

//        common.PhotometricInterpretation = 'MONOCHROME2';
        {"PhotometricInterpretation", "", "MONOCHROME2"},

//        try common.Private_0051_1011 = ['p' num2str(xprot{1}.x.PARC.PIPE.onlinetse_ps.iPATCalcStoreIndex.NAFLin)]; catch end;
//
//        % UIDs
//        % common.StudyInstanceUID = ''; % missing and necessary
//        common.StudyInstanceUID = dicomuid; % missing and necessary
        {"StudyInstanceUID", "", "", "study-uid"},

//        common.SeriesInstanceUID = dicomuid;
        {"SeriesInstanceUID", "", "", "series-uid"},

//        common.SeriesNumber = seriesnum;  % missing and necessary
        {"SeriesNumber", "", "", "series"},

//        common.StudyID = '11111111'; % missing and necessary
        {"StudyID", "", "11111111"},

//        % common.StudyID = '21523690'; % temporarily taked from PET image
//        common.AcquisitionNumber = 1;
        {"AcquisitionNumber", "", "1", ""},

        {"InstanceNumber", "", "", "slice"},

//
//        % not found
//        common.StudyDescription = '';
        {"StudyDescription"},

/*
        {"OperatorPhysicianName"},

//        common.OperatorPhysicianName.FamilyName = '';
        {"OperatorPhysicianName.FamilyName"},

//        common.OperatorPhysicianName.GivenName = '';
        {"OperatorPhysicianName.GivenName"},

//        common.OperatorPhysicianName.MiddleName = '';
        {"OperatorPhysicianName.MiddleName"},

//        common.OperatorPhysicianName.NamePrefix = '';
        {"OperatorPhysicianName.NamePrefix"},

//        common.OperatorPhysicianName.NameSuffix = '';
        {"OperatorPhysicianName.NameSuffix"},
*/
        {"ReferringPhysicianName"},

/*
//        common.ReferringPhysicianName.FamilyName = '';
        {"ReferringPhysicianName.FamilyName"},

//        common.ReferringPhysicianName.GivenName = '';
        {"ReferringPhysicianName.GivenName"},

//        common.ReferringPhysicianName.MiddleName = '';
        {"ReferringPhysicianName.MiddleName"},

//        common.ReferringPhysicianName.NamePrefix = '';
        {"ReferringPhysicianName.NamePrefix"},

//        common.ReferringPhysicianName.NameSuffix = '';
        {"ReferringPhysicianName.NameSuffix"},
*/

//        common.PercentSampling = 100;
        {"PercentSampling", "", "100"},

//        % not found patient info
//        common.PatientSize = 0;
        {"PatientSize", "", "","PatientSize"},

//        % common.AccessionNumber = '-1';
        {"AccessionNumber", "", "", "AccessionNumber"},
//
//        common.alFree = mrprot{1,1}.sWiPMemBlock.alFree;  % We want this to pass information from sequence


// Lines 392:399 of makeHeaders.m
//        headers{i,j,k} = common;
//        headers{i,j,k}.EchoNumber = j;
//        headers{i,j,k}.ImagePositionPatient = ImgPos';
//        headers{i,j,k}.ImageOrientationPatient = [ImgDir(1,:)';ImgDir(2,:)'];
//        headers{i,j,k}.Private_0019_1015 = headers{i,j,k}.ImagePositionPatient;
//        headers{i,j,k}.SliceLocation = center(3);%headers{i,j,k}.ImagePositionPatient(3);
//        headers{i,j,k}.InstanceNumber = l;l = l+1;
//        headers{i,j,k}.AcquisitionNumber = i;

        {"EchoNumbers", "", "1", ""},
        {"ImagePositionPatient", "", "", "ImagePositionPatient"},
        {"ImageOrientationPatient", "", "", "ImageOrientationPatient"},
        {"0019ImageOrientationPatient", "", "", "ImageOrientationPatient"},
        {"SliceLocation", "", "", "SliceLocation"},
//        {"InstanceNumber", "", "", "InstanceNumber"},
//        {"AcquisitionNumber", "", "", "AcquisitionNumber"},

        {"DateOfLastCalibration", "XProtocol.**.atTXCalibDate.ParamString"},
        {"TimeOfLastCalibration", "XProtocol.**.atTXCalibTime.ParamString"},

        {"VariableFlipAngleFlag", "", "N"},
        {"InPlanePhaseEncodingDirection", "", "COL"},
        {"AcquisitionMatrix", "", "", "AcquisitionMatrix"},

};
size_t dcmtag_number = sizeof(dcmtags) / sizeof(TagsLookupTable::dcmtag_entry_t);



#endif //SETDCMTAGS_DCMTAGS_HPP
