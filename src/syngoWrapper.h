//
// Created by Vadim on 10/23/15.
//

#ifndef SETDCMTAGS_SYNGOWRAPPER_H
#define SETDCMTAGS_SYNGOWRAPPER_H

#include <ostream>

#include "TagsLookupTable.h"

#include "../protpp/SyngoMRProtocol.hpp"


class SyngoWrapper {

public:
    /** Constructor, reads xprot tags from the raw data file
     * @param fname Raw data file name
     */
    SyngoWrapper(const char * fname);

    /** Prints avalable xprot tag names and values in human-readable format
     *  @param stream to write to
     */
    void printSyngoTags(std::ostream &stream);

    /** Prints avalable xprot tag names and values in human-readable format
     *  @param fname file name to write to
     */
    void printSyngoTags(const std::string &fname);

    /** Prints avalable xprot tag names and values as INI filet
     *  @param fname file name to write to
     */
    void printSyngoTagsToINI(const std::string &fname);

    /** Prints avalable xprot tag names and values as XML filet
     *  @param fname file name to write to
     */
    void printSyngoTagsToXML(const std::string &fname);

    /** Prints avalable xprot tag names and values as INFO filet
     *  @param fname file name to write to
     */
    void printSyngoTagsToINFO(const std::string &fname);

public:

    /** Iterates through tag_entries and sets its value field to the value of corresponding dattag (raw data file tag)
     *  @param tag_entries Lookup table holding DICOM tag names, their (default) values,
     *  raw data tags and calculated variables
     *  @param log_stream stream to log the errors in readable format
     *  @return returns true if all dattad were found, false otherwise
     */
    bool processDcmTags(TagsLookupTable::tag_entries_t &tag_entries, std::ostream &log_stream);

    /** Calculates variables mentioned in the Tags Lookup Table based on the raw datafile tags
     *  @param tag_variables Map where the variables and their calculated values are stored
     *  @param log_stream stream to log the errors in readable format
     *  @return returns true if all necessary dattad were found and variable calculation was successful, false otherwise
     */
    bool calcDcmtagVariables(TagsLookupTable::tag_variables_t & tag_variables, std::ostream & log_stream);

    /** Reads an xprot tag value
     *  @param dattag Raw data file tag name to be read
     *  @param[out] Read value
     *  @param log_stream stream to log the errors in readable format
     *  @return returns true if the dattad was found and read successfully, false otherwise
     */
    bool getTagValue(const char * dattag, std::string & value, std::ostream & log_stream);

    /** Reads an xprot tag value
     *  @param dattags Raw data file tag names to be read; first tag name found in the raw file sets the value
     *  @param[out] Read value
     *  @param log_stream stream to log the errors in readable format
     *  @return returns true if the dattad was found and read successfully, false otherwise
     */
    bool getTagValue(const std::vector<std::string>& dattags, std::string &value, std::ostream & log_stream);

        /** Reads an xprot tag value
         *  @param dattag Raw data file tag name to be read
         *  @param[out] Read value
         *  @param log_stream stream to log the errors in readable format
         *  @param default value set if the dattag was not found
         *  @return returns true if the dattad was found and read successfully, false otherwise
         */
    bool getTagValue(const char * dattag, double & value, std::ostream & log_stream, double default_value = 0);

    /** Reads mrprot tags from the raw data file
     * @param mrprot_tags Map to populate with read tag names and values
     *  @param default value set if the dattag was not found
     *  @param begin_marker String marking begin of the mrprot part in raw data file;
     *  it is unlikely that there could be a need to change it
     *  @param end_marker String marking end of the mrprot part in raw data file;
     *  it is unlikely that there could be a need to change it
     *  @return returns true if the tags were read successfully, false otherwise
     */
    bool read_mrprot(TagsLookupTable::mrprot_tags_t &mrprot_tags,
                     std::ostream &log_stream,
                     const std::string &begin_marker = "### ASCCONV BEGIN ",
                     const std::string &end_marker = "### ASCCONV END ###");

protected:
    SyngoMRProtocol smprot;
};

/// A recursive function to print boost::property_tree in a human readable format:
/// path.to.the.tag = [value]
template <class outstream_t>
void printTree (const boost::property_tree::ptree &pt, outstream_t & stream, const std::string & path = "") {
    if (pt.empty()) {
        stream << path << " = ["<< pt.data()<< "]"<< std::endl;
    } else {
        for (auto pos = pt.begin(); pos != pt.end(); ++pos) {
            printTree(pos->second, stream, path.empty() ? pos->first : path + "." + pos->first);
        }
    }
}


#endif //SETDCMTAGS_SYNGOWRAPPER_H
