
nSl=30;
nMapsContrasts=12;

in_path = '/Users/vadim/Projects-Cai2R/SetDCMTags/test-data/from_florian_mine/dcms/';
out_path = ''; %'/Users/vadim/Projects-Cai2R/SetDCMTags/test-data/from_florian_mine/dcms_in/'

for ii = 1:nSl
    for jj = 1:nMapsContrasts
 
        filename = ['mrf_recon_contrast',num2str(jj,'%02d'),'_slice',num2str(ii,'%02d'),'.dcm'];
        X = dicomread([in_path, filename]);   
%        meta = dicominfo([in_path, filename]);   
        dicomwrite(X,[out_path, filename]);
    end
end           
