#!/usr/bin/env bash

#MODULE_PATH=/Users/vadim/Library/Caches/CLion12/cmake/generated/786ec4f5/786ec4f5/Release/ PATH=${PATH}:../../shunit2/source/2.1/src/ ./test_SetDCMTags.sh
THE_APP=${MODULE_PATH}SetDCMTags

DATA_PATH=./test-suite/

TEMP_DIR="${DATA_PATH}temp"

RAW_FILE_NAME=meas_MID00530_FID397465_gre.dat
RAW_FILE=${DATA_PATH}${RAW_FILE_NAME}

ORIG_PATH=${DATA_PATH}original/
ORIG_CREAT_PATH=${DATA_PATH}orig-created/
CREAT_PATH=${DATA_PATH}created/

DCM_REGEX="example.slice(?'slice'\d*).dcm"

#-a /Users/vadim/Projects-Cai2R/yarramodules-setdcmtags/src/color_dicom.ini



#-----------------------------------------------------------------------------
# helper functions
#

checkReturn()
{
    rtrn=$1
    assertTrue "return code is not 0 ($rtrn)" $rtrn

    assertFalse "stderror shall be empty"  "[ -s ${stderrF} ]"
    assertFalse "stdout shall be empty"  "[ -s ${stdoutF} ]"
}

compareDCMs()
{
    for file in ${CREAT_PATH}*.dcm
    do
        fname="`basename $file`"
        dcmdump "${CREAT_PATH}${fname}" | grep -v UID | grep -v Time | grep -v Date > "${CREAT_PATH}${fname}.txt"
        dcmdump "${ORIG_CREAT_PATH}${fname}" | grep -v UID | grep -v Time | grep -v Date > "${ORIG_CREAT_PATH}${fname}.txt"

        cmp "${CREAT_PATH}${fname}" "${ORIG_CREAT_PATH}${fname}"
        diff "${CREAT_PATH}${fname}.txt" "${ORIG_CREAT_PATH}${fname}.txt" >  "${TEST_DIR}/diff.log"
        assertTrue "${fname} do not match the model" $?
    done
}
#-----------------------------------------------------------------------------
# test functions
#

testEmptyCommandLine()
{
    ${THE_APP} >"${stdoutF}" 2>"${stderrF}"

    rtrn=$?
    assertTrue "return code is not 0 ($rtrn)" $rtrn

    assertFalse "stderror shall be empty"  "[ -s ${stderrF} ]"

    grep "usage:" "$stdoutF" > /dev/null
    assertTrue "stdout shall contain usage message" $rtrn
}

testNormalRun()
{
    ${THE_APP} -r ${RAW_FILE} ${ORIG_PATH} ${CREAT_PATH} -x ${DCM_REGEX} >"${stdoutF}" 2>"${stderrF}"

    checkReturn $?

    compareDCMs
}

testNormalRunInPlace()
{
    cp ${ORIG_PATH}*.dcm ${CREAT_PATH}
    chmod a+w ${CREAT_PATH}*.dcm

    ${THE_APP} -r ${RAW_FILE}  ${CREAT_PATH} -x ${DCM_REGEX} >"${stdoutF}" 2>"${stderrF}"

    checkReturn $?

    compareDCMs
}

testRawFileTagsDump()
{
    ${THE_APP}  -r ${RAW_FILE} --write-xml ${TEST_DIR}/${RAW_FILE_NAME}.xml \
        --write-info ${TEST_DIR}/${RAW_FILE_NAME}.info \
        --write-txt ${TEST_DIR}/${RAW_FILE_NAME}.txt >"${stdoutF}" 2>"${stderrF}"

    checkReturn $?

    diff "${RAW_FILE}.xml" "${TEST_DIR}/${RAW_FILE_NAME}.xml" >  "${TEST_DIR}/xml_diff.log"
    assertTrue "xml dump do not match the model" $?

    diff "${RAW_FILE}.info" "${TEST_DIR}/${RAW_FILE_NAME}.info" >  "${TEST_DIR}/info_diff.log"
    assertTrue "info dump do not match the model" $?

    diff "${RAW_FILE}.txt" "${TEST_DIR}/${RAW_FILE_NAME}.txt" >  "${TEST_DIR}/txt_diff.log"
    assertTrue "txt dump do not match the model" $?
}

#-----------------------------------------------------------------------------
# suite functions
#

oneTimeSetUp()
{
  rm -fr "${TEMP_DIR}"
  mkdir "${TEMP_DIR}"
}

_oneTimeTearDown()
{
  rm -fr "${TEMP_DIR}"
}

setUp()
{
    TEST_DIR="${TEMP_DIR}/${_shunit_test_}"
    mkdir "${TEST_DIR}"

    stdoutF="${TEST_DIR}/stdout.log"
    stderrF="${TEST_DIR}/stderr.log"

    mkdir "${CREAT_PATH}"
}

tearDown()
{
  rm -fr "${CREAT_PATH}"
}

# load and run shUnit2
# shunit2 package shall be installed (Ubuntu) or
# the directory shunit2/source/2.1/src/ shall be in the system path (Mac OS), e.g. call the script as
# PATH=${PATH}:../../shunit2/source/2.1/src/ ./test_SetDCMTags.sh
. shunit2
#. ../../shunit2/source/2.1/src/shunit2

