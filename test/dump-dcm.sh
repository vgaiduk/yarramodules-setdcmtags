#!/usr/bin/env bash

OUT_PATH=$1

if [ -z "${OUT_PATH}" ]; then
  OUT_PATH=./*.dcm
fi

for file in $(find ${OUT_PATH}) ; do
  dcmdump $file > $file.txt
done