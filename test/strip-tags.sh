#!/usr/bin/env bash

######################################################################
#
# Open DICOMs in Matlab and write to DICOMs without metadata
#

IN_PATH=$1
OUT_PATH=$2

if [ -z "${OUT_PATH}" ]; then
  echo Usage: strip_tags.sh [input_file_mask] [output_path]
  exit 1
fi

if [ -z ${MATLAB} ]; then
  MATLAB=/usr/local/bin/matlab
fi

if [ -x ${MATLAB} ]; then
  echo Found Matlab executable at ${MATLAB}
else
  echo Variable MATLAB must be defined and point to the Matlab executable
  exit 1
fi

rm -R ${OUT_PATH}
mkdir ${OUT_PATH}
if [ $? != 0 ]; then
  echo Cannot create output path in ${OUT_PATH}
  exit 1
fi

for file in $(find ${IN_PATH}) ; do
#  echo $file
  outfile=${OUT_PATH}/$(basename $file)
  MATLAB_CMD="${MATLAB_CMD} X=dicomread('$file'); dicomwrite(X,'$outfile');"
done


if [ -n "${MATLAB_CMD}" ]; then
#  echo ${MATLAB_CMD}
  ${MATLAB} -nodesktop -nosplash -nojvm -r "${MATLAB_CMD} exit "
fi

