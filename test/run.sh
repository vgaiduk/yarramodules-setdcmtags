#!/usr/bin/env bash

######################################################################
#
# Setup variables
#

DATA_PATH=$1
if [ -n "$2" ]; then
  REGEX=$2
fi

if [ -z ${SETDCMTAGS} ]; then
  SETDCMTAGS=../build/SetDCMTags
fi

if [ -z ${DATA_PATH} ]; then
  echo Usage: run.sh [data_path]
  exit 1
fi

MEAS_PATH=${DATA_PATH}/meas
STRIPED_PATH=${DATA_PATH}/stripped
TAGS_PATH=${DATA_PATH}/tags

MEAS_FILE=$(find ${MEAS_PATH}/*.dat | head -1)

if [ -z ${REGEX} ]; then
  read -r REGEX < ${MEAS_PATH}/regex
fi

if [ -z ${REGEX} ]; then
  echo REGEX unknown
  exit 1
else
  echo Use regex=${REGEX}
fi

######################################################################
#
# Prepare test data
#

#TODO: copy the testset from remote location and unzip it to ${MEAS_PATH}

mmv "${MEAS_PATH}/*.IMA" "${MEAS_PATH}/#1.dcm"

./strip-tags.sh "${MEAS_PATH}/*.dcm" "${STRIPED_PATH}"
if [ $? != 0 ]; then
  exit 1
fi

./dump-dcm.sh "${MEAS_PATH}/*.dcm"

######################################################################
#
# Run the module
#

rm -R ${TAGS_PATH}
mkdir ${TAGS_PATH}

${SETDCMTAGS} -l -r ${MEAS_FILE} ${STRIPED_PATH}/ ${TAGS_PATH}/ -x ${REGEX} -wt

./dump-dcm.sh "${TAGS_PATH}/*.dcm"
